# cloud-misc (vultr)

This project is a collection of modules for performing various sundry cloud tasks in the Vultr cloud. For more information see the individual modules.

1. [caa](modules/caa)
2. [icloud-custom-domain](modules/icloud-custom-domain)
3. [gitlab-pages](modules/gitlab-pages)