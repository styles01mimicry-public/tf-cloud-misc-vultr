# caa
#
# This module creates the DNS records necessary for Certificate Authority
# Authorization (CAA).

# Create CAA issue records identifying trusted certificate issuers.

resource "vultr_dns_record" "issuer_caa" {
  data     = "0 issue \"${each.value}\""
  domain   = var.domain
  for_each = toset(var.issuers)
  name     = ""
  type     = "CAA"
  lifecycle {
    ignore_changes = [priority]
  }
}

# Create CAA issuewild records identifying trusted wildcard certificate
# issuers.

resource "vultr_dns_record" "wildcard_issuer_caa" {
  data     = "0 issuewild \"${each.value}\""
  domain   = var.domain
  for_each = toset(var.wildcard_issuers)
  name     = ""
  type     = "CAA"
  lifecycle {
    ignore_changes = [priority]
  }
}

# Create CAA IODEF records indicating where to send messages about problems
# issuing certificates.

resource "vultr_dns_record" "iodef_caa" {
  data     = "0 iodef \"${each.value}\""
  domain   = var.domain
  for_each = toset(var.iodefs)
  name     = ""
  type     = "CAA"
  lifecycle {
    ignore_changes = [priority]
  }
}
