variable "domain" {
  description = "The domain."
  type        = string
  validation {
    condition     = can(regex("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$", var.domain))
    error_message = "Invalid domain."
  }
}
variable "iodefs" {
  default     = []
  description = "A list of URLs for reporting issues."
  nullable    = false
  type        = list(string)
  validation {
    condition     = alltrue([for i in var.iodefs : can(regex("^(?:https?|mailto):", i))])
    error_message = "Invalid IODEF in IODEFs list."
  }
}
variable "issuers" {
  default     = [";"]
  description = "A list of trusted certificate issuers."
  nullable    = false
  type        = list(string)
  validation {
    condition = (
      var.issuers == tolist([";"]) ||
      alltrue([
        for i in var.issuers :
        can(regex("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$", i))
      ])
    )
    error_message = "Invalid entry in certificate issuers list."
  }
}
variable "wildcard_issuers" {
  default     = [";"]
  description = "A list of trusted wildcard certificate issuers."
  nullable    = false
  type        = list(string)
  validation {
    condition = (
      var.wildcard_issuers == tolist([";"]) ||
      alltrue([
        for i in var.wildcard_issuers :
        can(regex("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$", i))
      ])
    )
    error_message = "Invalid entry in wildcard certificate issuers list."
  }
}
