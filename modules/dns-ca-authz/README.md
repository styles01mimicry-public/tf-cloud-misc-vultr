# caa

This module creates the DNS records necessary for Certificate Authority Authorization (CAA).

## Parameters

|Parameter|Default|Description|
|---|---|---|
|`domain`||The domain name.|
|`issuers`|`[;]`|A list of trusted certificate issuers.|
|`wildcard_issuers`|`[;]`|A list of trusted wildcard certificate issuers.|
|`iodefs`|`[;]`|A list of URLs for reporting issues.|
