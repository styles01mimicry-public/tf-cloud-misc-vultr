# gitlab-pages

This module creates the DNS records necessary to validate domains for use with GitLab Pages.

## Parameters

|Parameter|Default|Description|
|---|---|---|
|`domain`||The base domain name (e.g. the `example.com` in `docs.example.com`.)|
|`gitlab_domain`||The GitLab Pages domain (e.g. `user.gitlab.com`.)|
|`subdomain`||The subdomain (e.g. the `docs` in `docs.example.com`.)|
|`verification_txt_record`||The verification TXT record provided at setup.|
