variable "domain" {
  description = "The base domain."
  type        = string
  validation {
    condition     = can(regex("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$", var.domain))
    error_message = "Invalid base domain."
  }
}
variable "gitlab_domain" {
  description = "The GitLab domain for the CNAME record."
  type        = string
  validation {
    condition     = can(regex("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$", var.gitlab_domain))
    error_message = "Invalid domain."
  }
}
variable "subdomain" {
  description = "The subdomain (for the CNAME.)"
  type        = string
  validation {
    condition     = can(regex("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$", var.subdomain))
    error_message = "Invalid subdomain."
  }
}
variable "verification_txt_record" {
  description = "The verification TXT record provided during setup."
  type        = string
}
