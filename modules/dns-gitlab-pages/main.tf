# vultr_dns_gitlab_pages
#
# This module creates the DNS records necessary to validate domains for use
# with GitLab Pages.

# Create the CNAME (or in future, ALIAS) record.

resource "vultr_dns_record" "domain_cname" {
  data   = var.gitlab_domain
  domain = var.domain
  name   = var.subdomain
  type   = "CNAME"
  lifecycle {
    ignore_changes = [priority]
  }
}

# Create the verification TXT record.

resource "vultr_dns_record" "domain_txt" {
  data   = "\"${var.verification_txt_record}\""
  domain = var.domain
  name   = "_gitlab-pages-verification-code.${var.subdomain}"
  type   = "TXT"
  lifecycle {
    ignore_changes = [priority]
  }
}
