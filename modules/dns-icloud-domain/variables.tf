variable "domain" {
  description = "The domain."
  type        = string
  validation {
    condition     = can(regex("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$", var.domain))
    error_message = "Invalid domain."
  }
}
variable "personal_txt_record" {
  description = "The personal TXT record provided during setup."
  type        = string
}
