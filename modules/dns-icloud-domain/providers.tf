terraform {
  required_providers {
    dns = {
      source  = "hashicorp/dns"
      version = ">= 3.3.2"
    }
    vultr = {
      source  = "vultr/vultr"
      version = ">= 2.15.1"
    }
  }
}
