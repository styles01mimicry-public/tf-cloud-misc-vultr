# icloud_custom_domain
#
# This module creates the DNS records necessary to use Custom Email Domain
# with iCloud Mail (see: https://support.apple.com/en-us/HT212514.)

# Set our MX record(s) to the iCloud Mail mail exchangers.

data "dns_mx_record_set" "icloud_mx" {
  domain = "icloud.com."
}
resource "vultr_dns_record" "domain_mx" {
  data   = each.value
  domain = var.domain
  for_each = toset([
    for i in data.dns_mx_record_set.icloud_mx.mx :
    trimsuffix(i.exchange, ".")
  ])
  name     = ""
  priority = 10
  type     = "MX"
}

# Set a "personal TXT record" as provided during setup, as well as the
# SPF/DKIM specific TXT/CNAME records.

resource "vultr_dns_record" "domain_personal_txt_record" {
  data   = format("\"%s\"", trim(var.personal_txt_record, "\""))
  domain = var.domain
  name   = ""
  type   = "TXT"
}
resource "vultr_dns_record" "domain_spf_txt" {
  data   = "\"v=spf1 include:icloud.com ~all\""
  domain = var.domain
  name   = ""
  type   = "TXT"
}
resource "vultr_dns_record" "domain_dkim_cname" {
  data   = "sig1.dkim.${var.domain}.at.icloudmailadmin.com"
  domain = var.domain
  name   = "sig1._domainkey"
  type   = "CNAME"
}
