# icloud-custom-domain

This module creates the DNS records necessary to use Custom Email Domain with iCloud Mail (see: [Use Custom Email Domain with iCloud Mail](https://support.apple.com/en-us/HT212514).)

## Parameters

|Parameter|Default|Description|
|---|---|---|
|`domain`||The domain name.|
|`personal_txt_record`||The personal TXT record provided during setup.|
